// dumb test
const error = {
  code: 401,
  message: 'Unauthorized'
};

const error2 = {
  code: 404,
  message: 'Not Found'
};

test('error should have message Unauthorized', () => {
  expect(error.message).toBe('Unauthorized');
  expect(error.code).toBe(401);
});

test('error should have message Not Found', () => {
  expect(error2.message).toBe('Not Found');
  expect(error2.code).toBe(424);
});